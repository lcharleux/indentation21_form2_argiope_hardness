# Formation Simulation Indentation Abaqus/Argiope/Hardness (Indentation 2021)

## Objectives of this training:
* Introduction to the Argiope and Hardness tools to perform automatic simulations under Abaqus with the GMSH mesh generator.
* Study of a practical case: is the determination of the equivalent modulus possible on a single indentation curve?
